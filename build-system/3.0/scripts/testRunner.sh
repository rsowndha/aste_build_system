DEFAULT_START_TEST=false
DEFAULT_PREPARE_TEST=false
DEFAULT_MAIN_CLASS=com.jjconsumer.dte.bdd.Stories

START_TEST=$DEFAULT_START_TEST
PREPARE_TEST=$DEFAULT_PREPARE_TEST
MAIN_CLASS=$DEFAULT_MAIN_CLASS

EMPTY_OPTION_PATTERN='ERROR: "%s" requires a non-empty option argument.\n'

while :; do
    case $1 in
        -h|-\?|--help)
            COMMAND_PATTERN=" %-30s | %s\n"
            printf 'Script is designed to prepare and run test \n'
            printf 'Usage:\n'
            printf "$COMMAND_PATTERN" '--path "path/to/artifact/folder"' 'Path to artifact folder. E.g.: --path "temp/canvas-tests-0.1.0"'
            printf "$COMMAND_PATTERN" '--prepareTest' 'If specified artifact content will be restructurized for test run.'
            printf "$COMMAND_PATTERN" '--overridingProperties "props"' 'Overriding properties list. E.g.: --overridingProperties "property1=value"'
            printf "$COMMAND_PATTERN" '--startTest' 'If specified test run will be started automaticly.'
            printf "$COMMAND_PATTERN" '--mainClass' 'Main class to start test'
            printf "$COMMAND_PATTERN" '--profile "profile"' 'Profile for test execution. E.g.: --profile "desktop/chrome"'
            printf "$COMMAND_PATTERN" '--encoding "encoding"' 'Encoding for test execution. E.g.: --encoding "UTF-8"'
            exit
            ;;
        --path)
            if [ -n "$2" ]
            then
                PATH_TO_ARTIFACT="$2"
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--path"
                exit 1
            fi
            ;;
        --prepareTest)
            PREPARE_TEST=true
            ;;
        --overridingProperties)
            if [ -n "$2" ]
            then
                OVERRIDING_PROPERTIES="$2"
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--overridingProperties"
                exit 1
            fi
            ;;
        --profile)
            if [ -n "$2" ]
            then
                PROFILE="$2"
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--profile"
                exit 1
            fi
            ;;
        --encoding)
            if [ -n "$2" ]
            then
                ENCODING="$2"
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--encoding"
                exit 1
            fi
            ;;
        --mainClass)
            if [ -n "$2" ]
            then
                MAIN_CLASS="$2"
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--mainClass"
                exit 1
            fi
            ;;
        --startTest)
            START_TEST=true
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1"
            ;;
        *)
            break
    esac

    shift
done

if [ -z "$PATH_TO_ARTIFACT" ]
then
    printf 'ERROR: Path to unpacked module is mandatory parameter. Please specify using "--path"\n'
    exit 1
fi

if [ ! -d "$PATH_TO_ARTIFACT" ]
then
    printf "Folder '%s' does not exist\n" "$PATH_TO_ARTIFACT"
    exit 1
fi

cd $PATH_TO_ARTIFACT
ARTIFACT_NAME=`find . -name "*.jar" -maxdepth 1 | head -1 | sed 's|./||'`

if $PREPARE_TEST
then
    printf "INFO: prepare test to run\n"
    mkdir aste-lib
    cd lib
    find . -regextype posix-extended -regex '.*/allure-adaptor-[0-9]+\.[0-9]+\.[0-9]+\.jar' -exec mv {} ../aste-lib \;
    cd ..
    mkdir classes
    cd classes
    jar -xfv ../$ARTIFACT_NAME
    cd ..
fi

if [ -n "$OVERRIDING_PROPERTIES" ] && [ -d "classes" ]
then
    cd classes
    echo "$OVERRIDING_PROPERTIES" > "overriding.properties"
    cd ..
fi

if $START_TEST
then
    printf "INFO: start test\n"
    cd ..
    rm -rf logs
    rm -rf output
    java -cp $PATH_TO_ARTIFACT/classes:$PATH_TO_ARTIFACT/aste-lib/*:$PATH_TO_ARTIFACT/lib/* `[ -n "$PROFILE" ] && echo -Dprofile="$PROFILE"` `[ -n "$ENCODING" ] && echo -Dfile.encoding="$ENCODING"` $MAIN_CLASS
fi
