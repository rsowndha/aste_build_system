DEFAULT_ARTIFACTORY_SERVER=https://epam:AP2R9wyMryCy8MVhd33xfnAzKMP@dev-hyperion-artifactory.jjc-devops.com/artifactory
DEFAULT_ARTIFACTORY_REPO=simple/libs-snapshot-local
DEFAULT_GROUP=com/asdna/aste

ARTIFACTORY_SERVER=$DEFAULT_ARTIFACTORY_SERVER
ARTIFACTORY_REPO=$DEFAULT_ARTIFACTORY_REPO
GROUP=$DEFAULT_GROUP

EMPTY_OPTION_PATTERN='ERROR: "%s" requires a non-empty option argument.\n'
TEST_RUN_PARAMS=()

while :; do

    [[  -z  "$1"  ]] && break

    case $1 in
        -h|-\?|--help)
            COMMAND_PATTERN=" %-30s | %s\n"
            printf 'Script is designed to resolve needed artifact and forward execution parameters to testRunner script.\n'
            printf 'Usage:\n'
            printf "$COMMAND_PATTERN" '--server "server/url"' 'Server url with credentials if needed. E.g.: --server "https://user:password@dev-hyperion-artifactory.jjc-devops.com/artifactory"'
            printf "$COMMAND_PATTERN" '--repo "repository"' 'Repository with artifact ("libs-snapshot-local" by default). E.g.: --repo "simple/libs-release-local"'
            printf "$COMMAND_PATTERN" '--group "group"' 'Folder with stored artifact ("com/asdna/aste" by default). E.g.: --group "com/asdna/aste"'
            printf "$COMMAND_PATTERN" '--name "artifact-name"' 'Name of artifact. This is mandatory parameter. E.g.: --name "hyperion-base-template"'
            printf "$COMMAND_PATTERN" '--testVersion "version"' 'Concrete version of artifact. Othervice latest will be used. E.g.: --testVersion "0.3.15"'
            exit
            ;;
        --server)
            if [ -n "$2" ]
            then
                ARTIFACTORY_SERVER=$2
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--server"
                exit 1
            fi
            ;;
        --repo)
            if [ -n "$2" ]
            then
                ARTIFACTORY_REPO=$2
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--repo"
                exit 1
            fi
            ;;
        --group)
            if [ -n "$2" ]
            then
                GROUP=$2
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--group"
                exit 1
            fi
            ;;
        --name)
            if [ -n "$2" ]
            then
                NAME=$2
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--name"
                exit 1
            fi
            ;;
        --testVersion)
            if [ -n "$2" ]
            then
                TEST_VERSION=$2
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--testVersion"
                exit 1
            fi
            ;;
        *)
            TEST_RUN_PARAMS+=("$1")
            ;;
    esac

    shift
done

# Check for consistency
if [ -z "$NAME" ]
then
    printf 'ERROR: Artifact name is mandatory parameter. Please specify using "--name"\n'
    exit 1
fi

ARTIFACT_PATH=$ARTIFACTORY_SERVER/$ARTIFACTORY_REPO/$GROUP/$NAME

if [ -n "$TEST_VERSION" ]
then
    VERSION=`curl -s $ARTIFACT_PATH/maven-metadata.xml | tac | grep $TEST_VERSION -m 1 | tac | sed "s/.*<version>\([^<]*\)<\/version>.*/\1/"`
else
    VERSION=`curl -s $ARTIFACT_PATH/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`
fi

if [ -z "$VERSION" ]
then
    printf 'ERROR: unable to find needed version\n'
    exit 1
fi

if [ "$ARTIFACTORY_REPO" = "$DEFAULT_ARTIFACTORY_REPO" ]
then 
    BUILD_NUMBER=`curl -s $ARTIFACT_PATH/$VERSION/maven-metadata.xml | grep '<value>' | head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"` 
else
    BUILD_NUMBER=$VERSION  
fi

ARTIFACT=$NAME-$BUILD_NUMBER.zip
ARTIFACT_FULL_NAME=$NAME-$VERSION
TEST_RUN_PARAMS+=("--path")
TEST_RUN_PARAMS+=("$ARTIFACT_FULL_NAME")

ARTIFACT_DOWNLOAD_LINK=$ARTIFACT_PATH/$VERSION/$ARTIFACT
printf "INFO: download test artifact from %s\n" ${ARTIFACT_DOWNLOAD_LINK/:\/\/*@/://*****@}
wget -q -N $ARTIFACT_DOWNLOAD_LINK
rm -rf $ARTIFACT_FULL_NAME
printf "INFO: unpack artifact\n"
jar -xfv $ARTIFACT
rm -f $ARTIFACT

sh $ARTIFACT_FULL_NAME/scripts/testRunner.sh "${TEST_RUN_PARAMS[@]}"

