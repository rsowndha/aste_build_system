printf "WARNING: script is deprecated. Use 'aste_build_system/ci/runTestFromArtifact.sh' instead\n"

DEFAULT_ARTIFACTORY_SERVER=https://epam:AP2R9wyMryCy8MVhd33xfnAzKMP@dev-hyperion-artifactory.jjc-devops.com/artifactory
DEFAULT_ARTIFACTORY_REPO=simple/libs-snapshot-local
DEFAULT_GROUP=com/asdna/aste
DEFAULT_START_TEST=false
DEFAULT_MAIN_CLASS=com.jjconsumer.dte.bdd.Stories

ARTIFACTORY_SERVER=$DEFAULT_ARTIFACTORY_SERVER
ARTIFACTORY_REPO=$DEFAULT_ARTIFACTORY_REPO
GROUP=$DEFAULT_GROUP
START_TEST=$DEFAULT_START_TEST
MAIN_CLASS=$DEFAULT_MAIN_CLASS

EMPTY_OPTION_PATTERN='ERROR: "%s" requires a non-empty option argument.\n'

while :; do
    case $1 in
        -h|-\?|--help)
            COMMAND_PATTERN=" %-30s | %s\n"
            printf 'Script is designed to prepare and update \n'
            printf 'Usage:\n'
            printf "$COMMAND_PATTERN" '--server "server/url"' 'Server url with credentials if needed. E.g.: --server "https://user:password@dev-hyperion-artifactory.jjc-devops.com/artifactory"'
            printf "$COMMAND_PATTERN" '--repo "repository"' 'Repository with artifact ("libs-snapshot-local" by default). E.g.: --repo "simple/libs-release-local"'
            printf "$COMMAND_PATTERN" '--group "group"' 'Folder with stored artifact ("com/jjconsumer/dte" by default). E.g.: --group "com/jjconsumer/dte"'
            printf "$COMMAND_PATTERN" '--name "artifact-name"' 'Name of artifact. This is mandatory parameter. E.g.: --name "dte-hyperion-template"\n'
            printf "$COMMAND_PATTERN" '--testVersion "version"' 'Concrete version of artifact. Othervice latest will be used. E.g.: --testVersion "0.3.15"'
            printf "$COMMAND_PATTERN" '--overridingProperties "props"' 'Overriding properties list. E.g.: --overridingProperties "property1=value"'
            printf "$COMMAND_PATTERN" '--startTest' 'If specified test run will be started automaticly.'
            printf "$COMMAND_PATTERN" '--mainClass' 'Main class to start test'
            printf "$COMMAND_PATTERN" '--profile "profile"' 'Profile for test execution. E.g.: --profile "desktop/chrome"'
            exit
            ;;
        --server)
            if [ -n "$2" ]
            then
                ARTIFACTORY_SERVER=$2
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--server"
                exit 1
            fi
            ;;
        --repo)
            if [ -n "$2" ]
            then
                ARTIFACTORY_REPO=$2
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--repo"
                exit 1
            fi
            ;;
        --group)
            if [ -n "$2" ]
            then
                GROUP=$2
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--group"
                exit 1
            fi
            ;;
        --name)
            if [ -n "$2" ]
            then
                NAME=$2
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--name"
                exit 1
            fi
            ;;
        --testVersion)
            if [ -n "$2" ]
            then
                TEST_VERSION=$2
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--testVersion"
                exit 1
            fi
            ;;
        --overridingProperties)
            if [ -n "$2" ]
            then
                OVERRIDING_PROPERTIES="$2"
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--overridingProperties"
                exit 1
            fi
            ;;
        --profile)
            if [ -n "$2" ]
            then
                PROFILE="$2"
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--profile"
                exit 1
            fi
            ;;
        --mainClass)
            if [ -n "$2" ]
            then
                MAIN_CLASS="$2"
                shift
            else
                printf "$EMPTY_OPTION_PATTERN" "--mainClass"
                exit 1
            fi
            ;;
        --startTest)
            START_TEST=true
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1"
            ;;
        *)
            break
    esac

    shift
done

# Check for consistency
if [ -z "$NAME" ]
then
    printf 'ERROR: Artifact name is mandatory parameter. Please specify using "--name"\n'
    exit 1
fi

ARTIFACT_PATH=$ARTIFACTORY_SERVER/$ARTIFACTORY_REPO/$GROUP/$NAME

if [ -n "$TEST_VERSION" ]
then
    VERSION=`curl -s $ARTIFACT_PATH/maven-metadata.xml | tac | grep $TEST_VERSION -m 1 | tac | sed "s/.*<version>\([^<]*\)<\/version>.*/\1/"`
else
    VERSION=`curl -s $ARTIFACT_PATH/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`
fi
if [ "$ARTIFACTORY_REPO" = "$DEFAULT_ARTIFACTORY_REPO" ]
then
    BUILD_NUMBER=`curl -s $ARTIFACT_PATH/$VERSION/maven-metadata.xml | grep '<value>' | head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`
else
    BUILD_NUMBER=$VERSION
fi

ARTIFACT=$NAME-$BUILD_NUMBER.zip
ARTIFACT_FULL_NAME=$NAME-$VERSION

rm -rf logs
rm -rf output
wget -q -N $ARTIFACT_PATH/$VERSION/$ARTIFACT
rm -rf $ARTIFACT_FULL_NAME
jar -xfv $ARTIFACT
rm -f $ARTIFACT
cd $ARTIFACT_FULL_NAME

mkdir aste-lib

cd lib
find . -regextype posix-extended -regex '.*/allure-adaptor-[0-9]+\.[0-9]+\.[0-9]+\.jar' -exec mv {} ../aste-lib \;
cd ..

mkdir classes
cd classes
jar -xfv ../$NAME-$VERSION.jar

if [ -n "$OVERRIDING_PROPERTIES" ]
then
    echo "$OVERRIDING_PROPERTIES" > "overriding.properties"
fi

if $START_TEST
then
    cd ../..
    java -cp $ARTIFACT_FULL_NAME/aste-lib/*:$ARTIFACT_FULL_NAME/lib/*:$ARTIFACT_FULL_NAME/classes `[ -n "$PROFILE" ] && echo -Dprofile="$PROFILE"` $MAIN_CLASS
fi
