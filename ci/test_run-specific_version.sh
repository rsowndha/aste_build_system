PLATFORM_URL="http://jjuser:jjpass1@jjsandboxjyftpbmiit.devcloud.acquia-sites.com"
ARTIFACT_NAME="dte-jjbosplatform-tests"
ARTIFACT_VERSION="0.1.5"

ARTIFACT_FULL_NAME=$ARTIFACT_NAME-$ARTIFACT_VERSION
BINARY_NAME=$ARTIFACT_FULL_NAME.zip
wget -q -N http://artifactory.jjc-devops.com:8383/artifactory/simple/libs-release-local/com/jjconsumer/dte/$ARTIFACT_NAME/$ARTIFACT_VERSION/$BINARY_NAME
rm -rf $ARTIFACT_FULL_NAME
jar -xfv $BINARY_NAME
cd $ARTIFACT_FULL_NAME
mkdir classes
cd classes
jar -xfv ../$ARTIFACT_FULL_NAME.jar
cat > "overriding.properties" <<EOL
com.jjconsumer.dte.selenium.WebDriverProvider/sauceLabsEnabled=true
com.jjconsumer.dte.jjbosplatform.steps.PlatformSteps/mainPlatformPageUrl=$PLATFORM_URL
EOL
cd ../..
java -cp $ARTIFACT_FULL_NAME/lib/*:$ARTIFACT_FULL_NAME/classes com.jjconsumer.dte.bdd.Stories